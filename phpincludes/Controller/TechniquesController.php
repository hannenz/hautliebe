<?php
namespace Hautliebe;

use Contentomat\Debug;
use Contentomat\Contentomat;
use Contentomat\Controller;
use Hautliebe\Technique;
use Hautliebe\AppController;
use \Exception;

class TechniquesController extends AppController {

	public function init () {
		parent::init();
		$this->Technique = new Technique();
		$this->Technique->order([
			'technique_pos' => 'asc'
		]);
	}



	public function index () {
		$techniques = $this->Technique->filter ([
			'technique_is_active' => true
		])->findAll ();
		$this->set(compact('techniques'));
	}
}
?>