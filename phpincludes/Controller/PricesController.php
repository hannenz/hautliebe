<?php
namespace Hautliebe;

use Hautliebe\Price;
use Hautliebe\Offer;
use Hautliebe\Treatment;
use Hautliebe\AppController;

class PricesController extends AppController {

	public function init () {
		parent::init();
		$this->Offer = new Offer ();
		$this->Price = new Price ();
		$this->Price->order([
			'price_pos' => 'asc'
		]);
		$this->Treatment = new Treatment ();
		$this->Treatment->order([
			'treatment_pos' => 'asc'
		]);
	}



	public function index() {

		$offers = $this->Offer->findAllActive ();
		$categories = $this->Treatment->filter ([
			'treatment_is_active' => true
		])->findAll ();

		foreach ($categories as &$category) {
			$category['prices'] = $this->Price->findByCategory ($category['id']);
		}

		$this->set(compact('offers', 'categories'));
	}
}
?>
