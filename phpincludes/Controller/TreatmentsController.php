<?php
namespace Hautliebe;

use Hautliebe\Treatment;
use Hautliebe\AppController;



class TreatmentsController extends AppController {

	public function init () {
		parent::init();

		$this->Treatment = new Treatment();
		$this->Treatment->order([
			'treatment_pos' => 'asc'
		]);
	}


	public function index() {
		$treatments = $this->Treatment->filter([
			'treatment_is_active' => true
		])->findAll ();
		$this->set(compact('treatments'));
	}
}
?>
