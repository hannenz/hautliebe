<!doctype html>
<html lang="{PAGELANG}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Hautliebe | Haarentfernung, Faltenbehandlung und Narbenbehandlung</title>
	<meta name="description" content="{PAGEVAR:cmt_meta_description:recursive}">
	<meta name="keywords" content="{PAGEVAR:cmt_meta_keywords:recursive}">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	{IF (!{LAYOUTMODE})}
		{INCLUDE:PATHTOWEBROOT . "templates/partials/no_pace.tpl"}
	{ENDIF}

	<link rel="shortcut icon" href="/dist/img/favicon.png" />
	<link rel="stylesheet" type="text/css" href="/dist/css/vendor/flickity.min.css" />
	<link rel="stylesheet" type="text/css" href="/dist/css/main.css" />
	<link rel="stylesheet" type="text/css" href="/dist/css/vendor/mapbox-gl.css" />

	{INCLUDE:PATHTOWEBROOT . "templates/partials/microdata.tpl"}
	{LAYOUTMODE_STARTSCRIPT}
	{IF (!{LAYOUTMODE})}
	<script src="/dist/js/vendor/jquery.min.js" defer></script>
	<script src="/dist/js/vendor/TweenMax.js" defer></script>
	<script src="/dist/js/vendor/ScrollMagic.min.js" defer></script>
	<script src="/dist/js/vendor/flickity.pkgd.min.js" defer></script>
	<script src="/dist/js/vendor/animation.gsap.min.js" defer></script>
	<script src="/dist/js/vendor/snap.svg-min.js" defer></script>
	<script src="/dist/js/vendor/mapbox-gl.js"></script>
	{ENDIF}
</head>
<body id="top">
	<!-- Inject SVG sprites -->
	<div style="visibility: hidden; height:0; overflow: hidden">
		<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><defs><clipPath clipPathUnits="userSpaceOnUse" id="clipPath905"><use height="100%" width="100%" y="0" x="0" style="overflow:visible" id="use907" overflow="visible" xlink:href="#SVGID_1_"/></clipPath><path id="SVGID_1_" d="M161.042 159.221h-38.621l-24.968 54.312h88.558z"/><clipPath id="SVGID_2_"><use xlink:href="#SVGID_1_" overflow="visible" id="use5" style="overflow:visible" x="0" y="0" width="100%" height="100%"/></clipPath><path id="SVGID_1_" d="M97.453 159.221h88.559v54.312H97.453z"/></defs><symbol id="etwo-sublative" viewBox="0 0 283.465 283.465"><path style="fill:none;stroke:#f5cdd2;stroke-width:22.43719292;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10" id="line2" stroke-miterlimit="10" d="M12.662 169.715h258.14"/><path style="fill:none;stroke:#f5cdd2;stroke-width:14;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10" id="polyline6" stroke-miterlimit="10" transform="translate(-85.416 -85.461) scale(1.60266)" d="M141.732 117.217l-44.279 96.316h88.558l-44.279-96.316"/><path style="fill:none;stroke:#f5cdd2;stroke-width:12.82125282;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10" d="M131.582 134.984L75.6 256.76" id="path876"/><path style="fill:none;stroke:#f5cdd2;stroke-width:12.82125282;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10" d="M139.862 170.133l-40.21 86.626" id="path872"/><path style="fill:none;stroke:#f5cdd2;stroke-width:12.82125282;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10" d="M166.725 163.185l-43.02 93.574" id="path866"/><path style="fill:none;stroke:#f5cdd2;stroke-width:12.82125282;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10" d="M179.15 188.476l-31.39 68.283" id="path860"/><path style="fill:none;stroke:#f5cdd2;stroke-width:12.82125282;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10" d="M191.577 213.766l-19.765 42.993" id="path854"/><path style="fill:none;stroke:#f5cdd2;stroke-width:14;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10" id="polyline36" stroke-miterlimit="10" transform="translate(-85.416 -85.461) scale(1.60266)" d="M117.482 70.182v46.5h48.5v-46.5"/></symbol><symbol id="etwo-sublime" viewBox="0 0 283.465 283.465"><path style="fill:none;stroke:#f5cdd2;stroke-width:12.74690437;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10" d="M102.776 167.523l-33.29 72.411" id="path952"/><path style="fill:none;stroke:#f5cdd2;stroke-width:12.74690437;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10" d="M126.69 167.523l-40.718 88.57" id="path946"/><path style="fill:none;stroke:#f5cdd2;stroke-width:12.74690437;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10" d="M150.603 167.523l-40.717 88.57" id="path940"/><path style="fill:none;stroke:#f5cdd2;stroke-width:12.74690437;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10" d="M176.681 167.523l-40.717 88.57" id="path934"/><path style="fill:none;stroke:#f5cdd2;stroke-width:12.74690437;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10" d="M200.599 167.522l-40.72 88.57" id="path928"/><path style="fill:none;stroke:#f5cdd2;stroke-width:12.74690437;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10" d="M212.287 194.119l-28.494 61.973" id="path922"/><path id="polyline36" style="fill:none;stroke:#f5cdd2;stroke-width:22.30708313;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10" d="M71.18 102.626v153.466h141.105V102.626M32.54 27.682v74.091h77.279V27.682m63.826 0v74.091h77.279V27.682M13.41 169.553h256.642"/></symbol><symbol id="gentlelase-pro" viewBox="0 0 283.465 283.465"><path stroke-miterlimit="10" id="polyline2" transform="translate(-84.469 -84.44) scale(1.59597)" style="fill:none;stroke:#f5cdd2;stroke-width:14;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10" d="M117.482 70.182v46.5h48.5v-46.5"/><path stroke-miterlimit="10" id="line4" style="fill:none;stroke:#f5cdd2;stroke-width:22.34364128;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10" d="M13.2 169.673h257.064"/><path stroke-miterlimit="10" id="line6" style="fill:none;stroke:#f5cdd2;stroke-width:22.34364128;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10" d="M141.732 102.636v119.405"/><path stroke-miterlimit="10" d="M109.299 210.557v12.55c0 18.141 14.521 32.848 32.433 32.848s32.433-14.707 32.433-32.848v-12.55" id="path8" style="fill:none;stroke:#f5cdd2;stroke-width:22.34364128;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10"/></symbol></svg>
		<!-- <object  -->
		<!-- 	type="image/svg+xml"  -->
		<!-- 	data="/dist/img/icons.svg"  -->
		<!-- 	onload="this.parentNode.replaceChild(HTMLObjectElement.contentDocument.childNodes[0], this)"> -->
		<!-- </object> -->
	</div>

	{INCLUDE:PATHTOWEBROOT.'templates/partials/header.tpl'}

	<div class="main-content">

		<section id="news" class="section section--1">
			<div class="outer-bound">
				{LOOP CONTENT(99)}{ENDLOOP CONTENT}

				<!-- <img class="eyecatcher eyecatcher&#45;&#45;discount" src="/dist/img/eyecatcher_discount.svg" title="&#45;15 % Rabatt &#45; nur für kurze Zeit" alt="Banner zur Neueröffnungsaktion mit der Aufschrift: 15 % Rabatt &#45; nur für kurze Zeit" /> -->
			</div>
		</section>


		<section id="treatments" class="section section--2 section--dark section--rotated-title">
			<!-- <div class="inner&#45;bound"> -->
			<!-- 	<div class="compare"> -->
			<!-- 		<img src="/dist/img/wrinkles&#45;before&#45;1.jpg" alt="" /> -->
			<!-- 		<img src="/dist/img/wrinkles&#45;after&#45;1.jpg" alt="" /> -->
			<!-- 	</div> -->
			<!-- </div> -->
			<div class="inner-bound">
				{LOOP CONTENT(1)}{ENDLOOP CONTENT}
			</div>
		</section>

		<section id="techniques" class="section section--3">
			<div class="inner-bound">
				{LOOP CONTENT(2)}{ENDLOOP CONTENT}
			</div>
		</section>

		<section class="divider-image">
			<img src="/media/GentleLasePro.webp" />
		</section>

		<section id="about" class="section section--4">

			<div class="outer-bound">
				<h2 class="title title--default">Über uns</h2>
			</div>

			<div class="outer-bound">
				{LOOP CONTENT(3)}{ENDLOOP CONTENT}
			</div>
		</section>

		<section class="divider-image">
			<img src="/media/hautliebe_finja_melanie.webp" />
		</section>

		<section id="pricing" class="section section--5 section--dark section--rotated-title">
			<div class="inner-bound">
				{LOOP CONTENT(4)}{ENDLOOP CONTENT}
				<!-- <img class="eyecatcher eyecatcher--freebie" src="/dist/img/eyecatcher_freebie.svg" title="Kostenlos: Erstberartungsgespräch und Testbehandlung (nicht für Sublative)" alt="Banner mit der Aufschrift: Kostenlos: Erstberatungsgespräch und Testbehandlung (nicht für Sublative)" /> -->
			</div>
		</section>

		<section id="appointments" class="section section--6">
			<div class="inner-bound">
				{LOOP CONTENT(5)}{ENDLOOP CONTENT}
			</div>
		</section>

		<section id="jobs" class="section section--dark section--rotated-title section--jobs">
			<div class="inner-bound">
				{LOOP CONTENT(7)}{ENDLOOP CONTENT}
			</div>
		</section>

		<section id="contact" class="section section--7 section--dark">
			<div class="inner-bound">
				{LOOP CONTENT(6)}{ENDLOOP CONTENT}
			</div>
		</section>

	</div>

	{INCLUDE:PATHTOWEBROOT.'templates/partials/footer.tpl'}
	<div>
		<section id="legal" class="section section--8 section--hidden">
			<div class="inner-bound">

				{LOOP CONTENT(999)}{ENDLOOP CONTENT}
				
				<a href="#top">Nach oben</a>
			</div>

		</section>
	</div>

	{IF(!{LAYOUTMODE})}
		<script src="/dist/js/main.js" defer></script>
	{ENDIF}
	{LAYOUTMODE_ENDSCRIPT}
</body>
</html>
