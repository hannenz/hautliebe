<div class="techniques">
	{LOOP VAR(techniques)}
	<div class="technique technique--{VAR:id}">
			<svg class="icon technique__icon"><use xlink:href="#{VAR:technique_icon}"></use></svg>
			<h2 class="headline technique__name">{VAR:technique_name}</h2>
			<div class="technique__columns">
				<div class="technique__description technique__description--1">{VAR:technique_description}</div>
				<div class="technique__description technique__description--2">{VAR:technique_description_2}</div>
			</div>
		</div>
	{ENDLOOP VAR}
</div>
