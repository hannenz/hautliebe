/**
 * src/js/main.js
 *
 * @author Johannes Braun
 * @package hautliebe
 *
 * main javascript file
 */

function APP () {

	var self = this;
	self.debug = false;
	self.mapHasBeenCreated = false;
	self.wrinklesIcons = [];
	self.scarsIcons = [];
	self.epilationIcons = [];
	self.offersIcons = [];

	var icons = document.querySelectorAll ('.icon--wrinkles');
	icons.forEach (function (icon) {
		self.wrinklesIcons.push (new WrinklesIcon (icon));
	});
	icons = document.querySelectorAll ('.icon--scars');
	icons.forEach (function (icon) {
		self.scarsIcons.push (new ScarsIcon (icon));
	});
	icons = document.querySelectorAll ('.icon--epilation');
	icons.forEach (function (icon) {
		self.epilationIcons.push (new EpilationIcon (icon));
	});
	icons = document.querySelectorAll ('.icon--offers');
	icons.forEach (function (icon) {
		self.offersIcons.push (new OffersIcon (icon));
	});

	this.init = function() {

		if (this.debug) {
			console.log('APP::init');
		}

		this.pageInit();
	};



	this.pageInit = function() {

		if (this.debug) {
			console.log('APP::pageInit');
		}

		document.body.classList.add('page-has-loaded');

		this.main();
	};



	/**
	 * ScrollSpy
	 * https://jsfiddle.net/mekwall/up4nu/
	 */
	this.scrollSpy = function () {
		// Cache selectors
		var lastId,
			topMenu = $(".main-nav"),
			topMenuHeight = 70; //topMenu.outerHeight() + 15,
			// All list items
			menuItems = topMenu.find("a"),
			// Anchors corresponding to menu items
			scrollItems = menuItems.map(function(){
				var item = $($(this).attr("href"));
				if (item.length) { 
					return item;
				}
			});

		// Bind click handler to menu items
		// so we can get a fancy scroll animation
		menuItems.click(function(e){
			var href = $(this).attr("href"),
				offsetTop = href === "#" ? 0 : $(href).offset().top - topMenuHeight + 1;
			$('html, body').stop().animate({ 
				scrollTop: offsetTop
			}, 300);
			e.preventDefault();
		});

		// Bind to scroll
		$(window).scroll(function(){
			// Get container scroll position
			var fromTop = $(this).scrollTop()+topMenuHeight;

			// Get id of current scroll item
			var cur = scrollItems.map(function(){
				if ($(this).offset().top < fromTop)
					return this;
			});
			// Get the id of the current element
			cur = cur[cur.length-1];
			var id = cur && cur.length ? cur[0].id : "";

			if (lastId !== id) {
				lastId = id;
				// Set/remove active class
				menuItems
					.parent().removeClass("active")
					.end().filter("[href='#"+id+"']").parent().addClass("active");
			} 

			// Close mobile menu
			document.body.classList.remove ('menu-is-open');
		});
	};



	this.createMap = function () {
		let mapElem = document.getElementById('map');
		const 
			lat = parseFloat(mapElem.getAttribute('data-latitude')),
			lng = parseFloat(mapElem.getAttribute('data-longitude')),
			zoom = parseInt(mapElem.getAttribute('data-zoom'));

		mapboxgl.accessToken = 'pk.eyJ1IjoiaGFubmVueiIsImEiOiJPMktpVm1BIn0.qMq_8uPobOFc-eBXIFVtog';
		var map = new mapboxgl.Map({
			'container': mapElem,
			'style': 'mapbox://styles/mapbox/light-v10',
			'center': [lng, lat],
			'zoom': zoom,
			'scrollZoom': false
		});
		map.addControl(new mapboxgl.NavigationControl());


		var pins = [
			{
				iconUrl: '/dist/img/pin_heart.png',
				// lngLat: [ 9.9930398, 48.3992668 ],
				lngLat: [ 9.9934698, 48.3991568 ],
				popup: "<b>HAUTLIEBE<sup>®</sup></b><br>Hafenbad&nbsp;1<br>89073 Ulm",
				iconSize: 120
			},
			{
				iconUrl: '/dist/img/pin_bus.png',
				lngLat: [ 9.99213, 48.40198 ],
				popup: "Haltestelle &bdquo;Justizgebäude&ldquo;",
				iconSize: 100
			},
			{
				iconUrl: '/dist/img/pin_parking1.png',
				lngLat: [ 9.99491, 48.40073 ],
				popup: "Parkhaus &bdquo;Kornhaus&ldquo;",
				iconSize: 100
			},
			{
				iconUrl: '/dist/img/pin_parking2.png',
				lngLat: [ 9.99604, 48.40111 ],
				popup: "Parkhaus &bdquo;Frauenstraße&ldquo;",
				iconSize: 100
			}
		];

		for (var i = 0; i < pins.length; i++) {
			const pin = pins[i];

			const popupOptions = {
				offset: [0, -pin.iconSize * 2 / 3]
			};

			const popup = new mapboxgl.Popup(popupOptions)
				.setLngLat(pin.lngLat)
				.setHTML(pin.popup)
			;
			if (i == 0) {
				// popup.openOn(map);
			}

			const el = document.createElement('div');
			el.style.backgroundImage = `url(${pin.iconUrl})`;
			el.style.width = `${pin.iconSize}px`;
			el.style.height = `${pin.iconSize}px`;
			el.style.backgroundSize = '100%';

			new mapboxgl.Marker({
					element: el,
					anchor: 'bottom'
				})
				.setLngLat(pin.lngLat)
				.setPopup(popup)
				.addTo(map);
		}
	};


	this.setupTabs = function () {
		var $wrapper = $('<div class="tabs__wrapper" />');
		$wrapper.insertAfter ($('#tab-3'));
		$wrapper.append ($('#tab-1'));
		$wrapper.append ($('#tab-2'));
		$wrapper.append ($('#tab-3'));

		$wrapper = $('<div class="tabs__wrapper" />');
		$wrapper.insertAfter ($('#tab-5'));
		$wrapper.append ($('#tab-4'));
		$wrapper.append ($('#tab-5'));

		$('.tabs').each (function (i, el) {
			var $el = $(el);
			$el.find('.tabs__trigger').on ('click', function (ev) {
				ev.stopPropagation ();
				$el.find ('.tabs__trigger').removeClass ('tabs__trigger--is-active');
				$el.find ('.tabs__panel').removeClass ('tabs__panel--is-active');
				$el.find ($(this).attr('href')).addClass ('tabs__panel--is-active');
				$(this).addClass ('tabs__trigger--is-active');

				self.wrinklesIcons.forEach (function (icon) {
					icon.stopAnimation ();
				});
				self.scarsIcons.forEach (function (icon) {
					icon.stopAnimation ();
				});
				self.epilationIcons.forEach (function (icon) {
					icon.stopAnimation ();
				});
				self.offersIcons.forEach (function (icon) {
					icon.stopAnimation ();
				});

				switch (this.getAttribute('href')) {
					case '#treatment-1':
					case '#price-category-1':
						self.scarsIcons.forEach (function (icon) {
							icon.animateInOut ();
						});
						break;
					case '#treatment-2':
					case '#price-category-2':
						self.wrinklesIcons.forEach (function (icon) {
							icon.animateInOut ();
						});
						break;
					case '#treatment-3':
					case '#price-category-3':
						self.epilationIcons.forEach (function (icon) {
							icon.animateOut ();
						});
						self.epilationIcons.forEach (function (icon) {
							icon.animateInOut ();
						});
						break;
					case '#price-offers':
						self.offersIcons.forEach (function (icon) {
							icon.animateInOut ();
						});
						break;
				}
				return false;
			});

			$el.find ('.tabs__trigger').first().addClass ('tabs__trigger--is-active');
			$el.find ('.tabs__panel').first().addClass ('tabs__panel--is-active');
			
		});

		// Initially start icon animations on page load
		self.epilationIcons[0].animateInOut ();
		self.offersIcons[0].animateInOut ();
	};

	this.setupHiddenSection = function () {
		
		var hiddenSection = document.querySelector ('.section--hidden');
		hiddenSection.classList.add ('section--is-hidden');
		var trigger = document.querySelector ('[href="#' + hiddenSection.id + '"]');
		trigger.addEventListener ('click', function () {
			hiddenSection.classList.toggle ('section--is-hidden');		
		});
	};

	this.main = function() {

		this.scrollSpy ();



		// Flickity carousel

		window.onload = function () {

			var nSlides = $('.carousel > *').length;
			var flickityOptions = {
				cellAlign: 'left',
				wrapAround: true,
				contain: true
			};
			if (nSlides <= 1) {
				flickityOptions.prevNextButtons = false;
				flickityOptions.pageDots = false;
			}
			$('.carousel').flickity(flickityOptions);

			// Sticky header
			new ScrollMagic.Scene ({
				offset: 120,
			})
			.setClassToggle (document.body, 'page-has-scrolled')
			.addTo (ctl);

			// TwentyTwenty: Comparison slider
			// $('.compare').twentytwenty ({
			// 	before_label: 'Vorher',
			// 	after_label: 'Nach 1 Behandlung'
			// });
		}



		// Load the map only when scrolling near to it

		var ctl = new ScrollMagic.Controller ();
		var loadMap = new ScrollMagic.Scene ({
			triggerElement: '#map',
			triggerHook: 1
		})
		.on ('enter', function () {
				if (!self.mapHasBeenCreated) {
					self.createMap ();
					self.mapHasBeenCreated = true;
				}
			}
		)
		.addTo (ctl);




		// Heart line animation (timeline)

		var heartLinePath = document.getElementById ('path814');
		var pathLength = heartLinePath.getTotalLength ();
		heartLinePath.style.strokeDasharray = pathLength + ' ' + pathLength;
		heartLinePath.style.strokeDashoffset = pathLength;
		var heartLineTween = 
			(TweenMax.to (heartLinePath, 1, { strokeDashoffset: 0, ease: Linear.easeNone }));

		var heartLine = new ScrollMagic.Scene ({
			triggerElement: '.about__timeline',
			triggerHook: 0.8,
			duration: 0, //"50%",
			tweenChanges: true
		})
		.setTween (heartLineTween)
		.addTo (ctl);

		var timelineStaggerTween = new TweenMax.staggerFromTo ('.timeline__text ul > li', 1,
			{ opacity: 0, x: 140 }, { opacity: 1, x: 0 },
			0.25
		);
		var timelineStagger = new ScrollMagic.Scene ({
			triggerElement: '.about__timeline',
			triggerHook: 1,
			duration: 0 //"75%"
		})
		.setTween (timelineStaggerTween)
		.addTo (ctl);


        // this.createWrinklesIcon ();
        // this.createScarIcon ();
        // this.createEpilationIcon ();
		this.setupTabs ();
		this.setupHiddenSection ();

	};
};

document.addEventListener('DOMContentLoaded', function() {

	var app = new APP();
	app.init();

});

